if empty(glob('~/.local/share/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'plasticboy/vim-markdown'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'chriskempson/base16-vim'
Plug 'ayu-theme/ayu-vim'
Plug 'itchyny/lightline.vim'
Plug 'chrisbra/Colorizer'
Plug 'jiangmiao/auto-pairs' 
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'junegunn/fzf', {'do': {-> fzf#install()}}
Plug 'junegunn/fzf.vim'
Plug 'alvan/vim-closetag'
Plug 'vim-syntastic/syntastic'
Plug 'rust-lang/rust.vim'
Plug 'vimwiki/vimwiki'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'tpope/vim-repeat'
Plug 'jasonshell/vim-svg-indent'
Plug 'dart-lang/dart-vim-plugin'
Plug 'guns/vim-sexp',    {'for': 'clojure'}
Plug 'liquidz/vim-iced', {'for': 'clojure'}
Plug 'liquidz/vim-iced-coc-source', {'for': 'clojure'}
Plug 'pappasam/nvim-repl'
Plug 'rescript-lang/vim-rescript'
Plug 'elixir-editors/vim-elixir'
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
Plug 'honza/vim-snippets'
Plug 'dense-analysis/ale'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
call plug#end()

set tabstop=2 softtabstop=2 expandtab shiftwidth=2 smarttab
autocmd FileType json syntax match Comment +\/\/.\+$+

"set guifont=Fira\ Code\ weight=453\ 12
set termguicolors
"colorscheme base16-tomorrow-night
let ayucolor="light"
colorscheme ayu
set hidden
"set nobackup
"set nowritebackup
"
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

set ignorecase
set smartcase
let mapleader=" "
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['<', '>'], ['{', '}']]

set splitbelow 
set laststatus=2
set noshowmode 

function! CocCurrentFunction()
  return get(b:, 'coc_current_function', '')
endfunction

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'gitbranch', 'readonly', 'filename', 'modified', 'currentfunction', 'cocstatus'] ],
        \   'right': [
          \              [ 'percent' ],
          \              [ 'fileformat', 'fileencoding', 'filetype' ],
          \             [ 'syntastic', 'lineinfo' ],
          \ ]
          \ },
          \ 'component_function': {
            \   'cocstatus': 'coc#status',
            \   'currentfunction': 'CocCurrentFunction',
            \   'gitbranch': 'FugitiveHead'
            \ },
            \ 'component_expand': {
              \   'syntastic': 'SyntasticStatuslineFlag',
              \ },
              \ 'component_type': {
                \   'syntastic': 'error',
                \ }
                \ }

function! SyntasticCheckHook(errors)
  call lightline#update()
endfunction

let g:colorizer_auto_filetype='css,html,javascript' 
let g:closetag_filenames = '*.re,*.html,*.xhtml,*.phtml,*.tsx,*.jsx,*.js'

let g:syntastic_error_symbol = '✖'
let g:syntastic_style_error_symbol = '✖'
let g:syntastic_warning_symbol = '!'
let g:syntastic_style_warning_symbol = '!'
let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_enable_highlighting = 1
"let g:syntastic_mode_map = {'mode': 'passive'}


let g:rustfmt_autosave = 1

augroup rainbow
  autocmd!
  autocmd FileType lisp,clojure,scheme,reason,rust,html,elixir RainbowParentheses
augroup END

nnoremap <silent><leader>g :GFiles<cr>
nnoremap <silent><leader>b :Buffers<cr>
nnoremap <silent><leader>\ :Explore<cr>
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always --smart-case --follow --hidden -g "!{node_modules/*,.git/*,.elixir-ls/*}" -- '.shellescape(<q-args>), 1,
      \   fzf#vim#with_preview(), <bang>0)

nnoremap <C-p> :Rg<Cr>
nnoremap <C-f> :Files<Cr>

inoremap <silent><expr> <TAB>
        \ coc#pum#visible() ? coc#pum#next(1):
        \ <SID>check_back_space() ? "\<Tab>" :
        \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
inoremap <silent><expr> <c-space> coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> <leader>n <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> <leader>d <Plug>(coc-definition)
nmap <silent> <leader>y <Plug>(coc-type-definition)
nmap <silent> <leader>i <Plug>(coc-implementation)
nmap <silent> <leader>u <Plug>(coc-references)
nmap <silent> <leader>f <Plug>(coc-format)
nmap <silent> <leader>r <Plug>(coc-refactor)
nmap <silent> <leader>v <Plug>(coc-rename)

nmap <silent> <leader>q :call CocActionAsync('showSignatureHelp')<cr> 
nmap <silent> <leader>o :call CocActionAsync('runCommand', 'tsserver.organizeImports')<cr>

" nmap <silent> <a-cr> :call CocAction<cr>
nmap <silent> <a-cr> <Plug>(coc-codeaction-selected)<cr>

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction 

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight') 

command! -nargs=0 Format :call CocAction('format')
nmap <silent <leader>f <Plug> :Format<cr>

vmap <leader>y :call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>
nmap <leader>V :call setreg("\"",system("xclip -o -selection clipboard"))<CR>p

" vim-slime configuration
let g:slime_target = "tmux"
let g:slime_python_ipython = 1

" Spell check config
:set spelllang=es

" Vim iced config
let g:iced_enable_default_key_mappings = v:true

" Elixir config
" Vim surrond
autocmd FileType eelixir let b:surround_37 ="<% \r %>"
autocmd FileType eelixir let b:surround_61 ="<%= \r %>"
autocmd FileType eelixir let b:surround_124 ="<%=  %> \r <% end %>"
autocmd FileType heex let b:surround_37 ="<% \r %>"
autocmd FileType heex let b:surround_61 ="<%= \r %>"
autocmd FileType heex let b:surround_124 ="<%=  %> \r <% end %>"
autocmd FileType elixir let b:surround_37 = "do \r end"
let g:ale_fixers = { 'elixir': ['mix_format'] }
